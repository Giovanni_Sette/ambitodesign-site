<?php get_search_form(); ?>

<div class="mt-[40px]">

    <h2 class="mb-[20px] text-[24px] font-bold"><?php _e("Articoli Recenti", "ambitodesign"); ?></h2>

    <?php
    $the_query = new WP_Query(array(
        'posts_per_page' => 3,
        'post_type' => 'post',
        'post_status' => 'publish',

    ));
    ?>

    <?php if ($the_query->have_posts()) : ?>
    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

    <a style="text-decoration-color:#FE7600;" class="border-solid border-l-[3px] border-white hover:border-orange hover:pl-[10px] duration-300 transition-all text-[16px] mb-[20px] font-light block group items-center"
        href="<?php get_post_permalink(the_permalink()); ?>"> <?php the_title(); ?>
    </a>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

    <?php else : ?>
    <p><?php _e("Non sono presenti articoli", "ambitodesign"); ?></p>
    <?php endif; ?>

    <a class="xl:mb-[40px] border-solid border-l-[3px] border-white hover:border-orange hover:pl-[10px] text-[18px] mt-[20px] duration-300 transition-all block uppercase group font-bold items-center"
        href="<?php echo get_permalink(get_option('page_for_posts')); ?>">
        <?php _e("Leggi tutti gli articoli", "ambitodesign"); ?>
    </a>

</div>

<div class="relative text-white bg-green-900">

    <div class="z-[0] absolute w-full h-full bg-gray-500 bg-opacity-20"></div>

    <div class="hidden xl:block p-[20px] z-[1] relative ">
        <h2 class="text-white font-bold mb-[25px] text-[22px] leading-[1.3]">Contattaci subito <br> per ottenere il tuo <br>sito web</h2>
        <p class="text-white font-medium text-[16px] leading-[1.4] mb-[30px]">Risparmia oltre il 10% se ci concludi entro fine anno.</p>
        <a class="button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(13); ?>"><?php _e("Contattaci ora", "ambitodesign"); ?></a>
    </div>

</div>