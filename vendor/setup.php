<?php
function my_myme_types($mime_types){
	$mime_types['svg'] = 'image/svg+xml';
	$mime_types['webp'] = 'image/webp'; 
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

function logo_login() { ?>

<style type="text/css">
body.login div#login h1 a {
    background-image: url('<?php echo wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' )[0]; ?>');
    padding-bottom: 30px;
    width: auto;
    background-size: contain;
}

.login #backtoblog a,
.login #nav a {
    color: #fe7600 !important;
}

.login .message {
    border-left: 4px solid #fe7600 !important;
}

input[type=text]:focus,
input[type=password]:focus,
input[type=checkbox]:focus {
    border-color: #fe7600 !important;
    box-shadow: 0 0 0 1px #fe7600 !important;
}

input[type=checkbox]:checked::before {
    @apply  !grayscale;

}

.wp-core-ui .button-primary {
    background-color: #fe7600 !important;
    border-color: #fe7600 !important;
}
</style>
<?php 
} add_action( 'login_enqueue_scripts', 'logo_login' );

show_admin_bar(false);

function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

function add_additional_class_on_link( $atts, $item, $args ) {
	if(isset($args->add_link_class)) {
        $atts['class'] = $args->add_link_class;
    }
    return $atts;
}

add_filter( 'nav_menu_link_attributes', 'add_additional_class_on_link', 10, 3 );


add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point($path)
{

    // update path
    $path = get_template_directory() . '/vendor/acf';
    if (!file_exists($path)) {
        mkdir($path);
    }

    return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point($paths)
{

    // remove original path (optional)
    unset($paths[0]);

    $paths[] = get_template_directory() . '/vendor/acf';


    // return
    return $paths;
}