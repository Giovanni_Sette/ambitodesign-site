<?php
add_action('init', function () {

    register_post_type(
        'portfolio',
        array(
            'labels' => array(
                'name' => __('Portfolio', 'cesarini'),
                'singular_name' => __('Portfolio', 'cesarini'),
                'add_new' => 'New Project',
                'add_new_item' => 'Add new Project',
                'new_item' => 'New Project',
                'edit_item' => 'Edit Project',
                'view_item' => 'See Project',
                'all_items' => 'All Projects',
                'search_items' => 'Search Project',
                'not_found' => 'Project not found'

            ),
            'public' => true,
            'has_archive' => true,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-portfolio',
        )
    );

});

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init()
{
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title'    => 'Archive events',
            'menu_title'    => 'Archive events',
            'parent_slug'   => 'edit.php?post_type=portfolio'
        ));

    }
}