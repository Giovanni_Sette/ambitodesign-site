const plugin = require('tailwindcss/plugin')

module.exports = {
  mode: 'jit',
  theme: {
    extend: {
      boxShadow: {
        'cta_orange': '7px 5px 10px #FE760099',
        'cta_green': '7px 5px 10px #209C4E99',
        'cta_social': '-1px 1px 8px #d6d6d6',
        'cta_contatti': '0px 4px 15px 4px #FE760099'
      },
      colors: {
        orange: {
          DEFAULT: '#FE7600',
        },
        green: {
          DEFAULT: '#209C4E',
        },
        black: {
          DEFAULT: '#1a1a1a',
        }
      }
    },
    container: {
      center: true,
    },

    fontFamily: {
      'poppins': ['Poppins', 'sans-serif'],
      'Expressa-DemiBold': ['Expressa-DemiBold', 'sans-serif']
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    plugin(function ({ addUtilities }) {
      const newUtilities = {
        '.bg-0': {
          'background': 'none'
        },
      }

      addUtilities(newUtilities, ['responsive', 'hover'])
    })
  ],
  purge: {
    content: [
      './templates/*.php',
      './components/*.php',
      './assets/**/*.js',
      './*.php'
    ]
  }
}
