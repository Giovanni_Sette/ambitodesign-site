<?php get_header(); ?>

<div class="container responsive-container ">
    <div class="flex flex-col xl:flex-row xl:gap-x-[50px]">
        <div class="w-full xl:w-3/4">

            <div class="min-h-[640px] md:min-h-[720px] h-[100vh] md:h-[70vh] xl:h-[100vh] pt-[100px] pb-[80px]">

                <div class="h-full group bg-center bg-cover relative" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">

                    <h3 class="z-[2] left-[30px] top-[30px] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180" style="writing-mode: vertical-lr">
                        <?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?>
                    </h3>

                    <div class="z-[0] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>

                    <div class="z-[1] absolute bottom-[30px] left-[30px] text-white">
                        <p class="mb-[10px] text-white inline-block border-solid border-white border-b-2 pb-[10px] text-[16px]">
                            <?php echo get_the_date(); ?>
                        </p>
                        <h1 class="text-white w-4/5 font-bold text-[25px] md:text-[35px] leading-[1.4]"><?php the_title(); ?></h1>
                    </div>
                    <a href="#post">
                        <div class="z-[1] absolute bottom-[30px] right-[30px] text-white rotate-180"><i class="fas fa-arrow-up text-[35px] ctapostarrow"></i></div>
                    </a>
                </div>

            </div>

            <a id="post" class="block relative top-[-100px] invisible"></a>

            <div class="single-article text-[18px] md:text-[20px] prose">
                <?php the_content(); ?>
            </div>

            <div class="mt-[55px]">
                <h4 class="text-[18px] text-orange  uppercase font-semibold mb-[10px]"><?php _e("Condividi questo articolo", "ambitodesign") ?></h4>
                <div class="flex space-x-[15px] items-baseline text-[30px]">
                    <a class="group" href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank" rel="noopener noreferrer">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/facebook.png" alt="">
                    </a>
                    <a class="group" href="https://api.whatsapp.com/send?text=<?php echo get_the_title(); ?>%20<?php echo get_permalink(); ?>" target="_blank" rel="noopener noreferrer">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/whatsapp.png" alt="">
                    </a>
                    <a class="group" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo get_permalink(); ?>" target="_blank" rel="noopener noreferrer">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/linkedin.png" alt="linkedin">
                    </a>
                    <a class="group" href="mailto:?subject=<?php echo get_the_title(); ?>&body=<?php echo get_permalink(); ?>" target="_blank" rel="noopener noreferrer">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="gmail">
                    </a>



                </div>
            </div>

            <div class="prev-next-posts flex justify-between mt-[70px]">

                <div class="w-1/2 pr-8 xl:pr-[144px]">
                    <?php $prev_post = get_adjacent_post(false, '', true);
                    if (!empty($prev_post)) { ?>
                    <h4 class="post-prev font-bold text-[14px] mb-[24px] uppercase"><?php _e("Post Precedente", "ambitodesign"); ?></h4>
                    <a class="border-solid border-l-[3px] border-white hover:border-orange hover:pl-[10px] transition-all duration-300 flex group items-center" href="<?php echo get_permalink($prev_post->ID); ?>"
                        title="<?php echo $prev_post->post_title; ?>">
                        <span class="text-[16px]"><?php echo $prev_post->post_title; ?></span>
                    </a>
                    <?php } ?>
                </div>

                <div class="w-1/2 pl-4 xl:pl-[144px]">
                    <?php $next_post = get_adjacent_post(false, '', false);
                    if (!empty($next_post)) { ?>
                    <h4 class="post-next font-bold text-[14px] mb-[24px] uppercase text-right"><?php _e("Post Successivo", "ambitodesign"); ?></h4>
                    <a class="text-right border-solid border-r-[3px] border-white hover:border-orange hover:pr-[10px] transition-all duration-300 flex justify-end group items-center" href="<?php echo get_permalink($next_post->ID); ?>"
                        title="<?php echo $next_post->post_title; ?>">
                        <span class="text-[16px]"><?php echo $next_post->post_title; ?></span> </a>
                    <?php } ?>
                </div>

            </div>

        </div>

        <div class="w-full xl:w-1/4 mt-[50px] xl:mt-0 xl:pt-[100px]">
            <?php include(locate_template('sidebar.php')); ?>
        </div>
    </div>

    <?php 
        $style = "background: rgb(0,129,19);background: linear-gradient(103deg, rgba(0,129,19,1) 0%, rgba(20,163,80,1) 29%, rgba(93,111,254,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div>

<?php get_footer();