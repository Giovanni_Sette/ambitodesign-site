jQuery(function ($) {

  $(document).ready(function () {

    $('#preloader').delay(1000).fadeOut('slow');

    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $(".main-menu").parents(".fixed-mobile").toggleClass('invisible opacity-0');
      $("html").toggleClass('overflow-hidden');
    });

    $('.close-modal').click(function () {
      $(this).addClass('closed');
      $(this).parent().parent('.modal').fadeToggle();
      $("html").removeClass('overflow-hidden');
    });

    $('.modal-portfolio').click(function () {
      $(this).closest('.card').find('.modal').fadeToggle().find('.close-modal').removeClass('closed');
      $("html").addClass('overflow-hidden');
    });

    /*
    
    $("li.menu-item a:contains('Homepage')").mouseover(function () {
      $('img.img-header').attr('src', '<?php echo get_template_directory_uri(); ?>/dist/img/homepage/hero.jpg');
    })

    $("li.menu-item a:contains('La nostra storia')").mouseover(function () {
      $('img.img-header').attr('src', '<?php echo get_template_directory_uri(); ?>/dist/img/homepage/team.jpg');
    })

    $("li.menu-item a:contains('Progetti')").mouseover(function () {
      $('img.img-header').attr('src', 'url(#portfolio)');
    })

    $("li.menu-item a:contains('Blog')").mouseover(function () {
      $('img.img-header').attr('src', 'url(#blog)');
    })

    $("li.menu-item a:contains('Contatti')").mouseover(function () {
      $('img.img-header').attr('src', 'url(#contatti)');
    })

    */

  });
});

