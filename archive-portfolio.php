<?php /* Template Name: Portfolio */ get_header(); ?>

<div class="container responsive-container pt-[100px]">

    <!-- HERO -->
    <div class="mt-[25px] xl:mt-[75px] mb-[75px] xl:mb-[125px] text-center">
        <h1 class="text-[40px] md:text-[50px] font-bold mx-auto w-full lg:w-3/4 xl:w-3/5 leading-[1.3]">
            Ascoltiamo la tua <span class="text-orange">idea</span> <br class="hidden md:block"> e la rendiamo reale.
        </h1>
        <p class="mb-[50px] text-[18px] md:text-[20px] mt-[15px] font-medium leading-[1.4]">
            Scopri più informazioni su ogni progetto cliccando sulla relativa box.
        </p>
    </div>

    <!-- GRID -->
    <div class="mb-[50px] w-full mx-auto gap-y-[50px] md:gap-[25px] lg:gap-[50px] grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3">
        <?php
            $args = array(
                'post_type' => 'portfolio',
                'posts_per_page' => -1,
                'post_status' => 'publish',
            );
            $query = new WP_Query($args);
            if ($query->have_posts()) : ?>
        <?php while ($query->have_posts()) : $query->the_post(); ?>

        <?php 
            $excerpt = get_field('excerpt_portfolio');
            $bg_image = get_field('immagine_sfondo_portfolio');
        ?>

        <div class="card">

            <div class="modal-portfolio group">

                <div class="relative h-0 pb-[100%]">
                    <div class="absolute inset-0 w-full h-full">
                        <div class="relative h-full w-full bg-center bg-cover" style="background-image:url('<?php echo esc_url($bg_image['url']); ?>')">
                            <div class="z-[0] group-hover:opacity-50 absolute w-full h-full bg-black opacity-0 transition-all duration-700">
                            </div>
                            <div class="px-[50px] h-full relative z-[1] opacity-0 group-hover:opacity-100 transition-all duration-300 text-center flex flex-col justify-center items-center">
                                <a class="hover:bg-white hover:text-orange text-white bg-orange rounded-full w-[60px] h-[60px] flex items-center justify-center text-[20px] transition-all duration-500">
                                    <i class=" fas fa-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-between items-center pt-[13px]">
                    <div class="px-[15px] border-solid border-l-[4px] border-orange">
                        <h2 class="text-[25px] md:text-[35px] font-bold leading-[1.2]"><?php the_title(); ?></h2>
                        <?php if ($excerpt) : ?>
                        <p class="text-[14px] md:text-[16px] mt-[5px] leading-[1.4]"><?php echo $excerpt; ?></p>
                        <?php endif; ?>
                    </div>
                    <i class="group-hover:-rotate-90 transition-all duration-300 mr-[19px] text-[38px] text-orange fas fa-chevron-right"></i>
                </div>

            </div>

            <?php 
                $sottotitolo = get_field('sottotitolo_portfolio');
                $task = get_field('task_portfolio');
                $obiettivi = get_field('obiettivi_task');
                $image_collaborazione = get_field('image_collaborazione');
                $text_performance = get_field('performance_e_risultati');
                $strumenti_utilizzati = get_field('strumenti_utilizzati');
                $link = get_field('link_sito');

                $grid_images = get_field('images_portfolio');
            ?>
            <div class="modal hidden w-full bg-black bg-opacity-40 fixed z-[120] inset-0 pt-[100px] px-[1.5rem] md:px-[3rem]">

                <div class="relative container">
                    <button class="z-[10] absolute top-[-20px] left-[-20px] rounded-xl flex items-center justify-center bg-orange w-[50px] h-[50px] close-modal" type="button">
                        <span class="!bg-white"></span>
                        <span class="!bg-white"></span>
                        <span class="!bg-white"></span>
                    </button>
                    <div class="overflow-y-scroll bg-white px-[1.5rem] py-[3rem] md:p-[3rem] h-[80vh] md:h-[70vh] xl:min-h-[600px] xl:h-[85vh]">
                        <div class="md:pl-[15px] border-solid md:border-l-[4px] border-orange">
                            <h2 class="text-[30px] md:text-[40px] font-bold leading-[1.2]"><?php the_title(); ?></h2>
                            <?php if($sottotitolo) : ?>
                            <p class="text-[18px] mt-[10px] leading-[1.4]"><?php echo $sottotitolo; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="flex flex-col lg:flex-row lg:space-x-[30px]">
                            <div class="w-full lg:w-1/2">
                                <?php if($task) : ?>
                                <div class="mt-[30px] md:mt-[60px]">
                                    <h3 class="text-[24px] font-bold leading-[1.2] mb-[10px]">Di cosa ci siamo occupati:
                                    </h3>
                                    <p class="text-[18px] leading-[1.4] prose portfolio-task xl:pr-[50px]">
                                        <?php echo $task; ?>
                                    </p>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="w-full lg:w-1/2">
                                <?php if($obiettivi) : ?>
                                <div class="mt-[30px] md:mt-[60px]">
                                    <h3 class="text-[24px] font-bold leading-[1.2] mb-[10px]">Obiettivi del Progetto:
                                    </h3>
                                    <p class="text-[18px] leading-[1.4] prose portfolio-task xl:pr-[50px]">
                                        <?php echo $obiettivi; ?>
                                    </p>
                                </div>
                                <?php endif; ?>

                            </div>





                        </div>
                        <div class="mt-[30px] lg:mt-[60px] w-full h-auto mx-auto gap-y-[10px] md:gap-[10px] grid grid-cols-1 md:grid-cols-3">
                            <?php
                $grid_large = $grid_images['large_image_grid_portfolio']['url'];
                $grid_small = $grid_images['small_image_grid_portfolio']['url'];
            ?>

                            <?php if($grid_large) : ?>
                            <div class="col-span-2 relative h-0 pb-[56.25%]">

                                <img class="w-full h-full absolute inset-0 object-cover" src="<?php echo esc_url( $grid_large ); ?>" alt="<?php echo esc_attr($grid_large); ?>">
                            </div>
                            <?php endif; ?>

                            <?php if($grid_small) : ?>

                            <img class="w-full h-full object-cover" src="<?php echo esc_url( $grid_small ); ?>" alt="<?php echo esc_attr($grid_small); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="flex flex-col lg:flex-row mt-[30px] md:mt-[60px] lg:space-x-[30px] space-y-[30px] md:space-y-[60px] lg:space-y-0">
                            <div class="w-full lg:w-2/3 xl:3/4">
                                <?php if($text_performance) : ?>
                                <div>
                                    <h3 class="text-[24px] font-bold leading-[1.2] mb-[10px]">Performance e Risultati:
                                    </h3>
                                    <p class="text-[18px] leading-[1.4] prose portfolio-task xl:pr-[50px]">
                                        <?php echo $text_performance; ?>
                                    </p>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="w-full lg:w-1/3 xl:1/4 flex flex-col justify-between">
                                <?php if($strumenti_utilizzati) : ?>
                                <div>
                                    <h3 class="text-[24px] font-bold leading-[1.2] mb-[10px]">Strumenti utilizzati:</h3>
                                    <div class="mt-[15px] mb-[40px] flex space-x-[15px] items-center text-[40px]">
                                        <?php 
                        if($strumenti_utilizzati['html']) : 
                            echo $strumenti_utilizzati['html']; 
                        endif; 

                        if($strumenti_utilizzati['css']) : 
                            echo $strumenti_utilizzati['css']; 
                        endif; 

                        if($strumenti_utilizzati['js']) : 
                            echo $strumenti_utilizzati['js']; 
                        endif; 

                        if($strumenti_utilizzati['nodejs']) : 
                            echo $strumenti_utilizzati['nodejs']; 
                        endif; 

                        if($strumenti_utilizzati['php']) : 
                            echo $strumenti_utilizzati['php']; 
                        endif; 
                        ?>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php if($link) : ?>
                                <a href="<?php echo esc_url( $link['url']); ?>" style="width:fit-content" class="flex items-center border-b-[2px] border-black border-solid pb-[4px] text-[24px] font-bold leading-[1.2]" target="__blank">
                                    <?php _e("Visita il sito","ambitodesign"); ?>
                                    <i class="text-[18px] fas fa-external-link-alt ml-[5px]"></i>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php
                            $grid_small_left = $grid_images['small_left_image_portfolio']['url'];
                            $grid_small_right = $grid_images['small_right_image_portfolio']['url'];

                            if(isset($grid_small_left) && isset($grid_small_right)) :
                        ?>
                        <div class="w-full flex md:flex-row flex-col lg:mt-[50px] space-y-[10px] md:space-y-0 md:space-x-[10px] md:h-[350px]">

                            <img class="w-full md:w-1/2 h-full object-cover" src="<?php echo esc_url($grid_small_left); ?>" alt="<?php echo esc_attr($grid_small_left); ?>">

                            <img class="w-full md:w-1/2 h-full object-cover" src=" <?php echo esc_url($grid_small_right); ?>" alt="<?php echo esc_attr($grid_small_right); ?>">

                        </div>
                        <?php endif; ?>
                        <div class="w-full">
                            <?php if($image_collaborazione) : ?>
                            <h3 class="mt-[30px] md:mt-[60px] text-[24px] font-bold leading-[1.2] mb-[10px]">In collaborazione con:
                            </h3>
                            <img class="mt-[15px] h-[50px]" src="<?php echo esc_url($image_collaborazione['url']); ?>" alt="<?php echo esc_attr($image_collaborazione['alt']); ?>">
                            <?php endif; ?>
                        </div>


                    </div>

                </div>

            </div>
        </div>
        <?php
            endwhile;
            wp_reset_postdata(); 
            else : ?>
        <p class="text-center text-[22px] md:text-[32px] leading-[30px] md:leading-[40px] tracking-[.68px] md:tracking-[.99px] mx-auto">
            <?php _e("Non sono presenti progetti", "ambitodesign"); ?>
        </p>
        <?php endif; ?>

    </div>
    <!-- FINE GRID -->

    <?php 
        $style = "background: rgb(80,23,159);background: linear-gradient(77deg, rgba(80,23,159,1) 0%, rgba(182,40,168,1) 53%, rgba(232,41,154,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div>

<?php get_footer(); ?>