<?php get_header(); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
    if($paged == 1 ) : ?>
<div class="hidden lg:block container responsive-container">

    <?php
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'post_status' => 'publish',
    );
    $featuredQuery = new WP_Query($args);
    $cont = 0;
    if ($featuredQuery->have_posts()) : ?>

    <div class="flex flex-col lg:flex-row lg:h-[70vh] xl:h-[100vh] min-h-[640px] md:min-h-[720px] pt-[100px] lg:py-[100px]">
        <?php while ($featuredQuery->have_posts()) : $featuredQuery->the_post(); 
        if($cont == 0) { ?>
        <div class="group w-full lg:w-3/5 bg-center bg-cover relative" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
            <h3 class="z-[2] left-[30px] top-[30px] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180" style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
            <div class="z-[0] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
            <div class="z-[1] absolute bottom-0 p-[30px] text-white">
                <h2 class="text-white w-2/3 font-bold text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
                <a class="mt-[30px] button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>">
                    <?php _e("leggi tutto", "ambitodesign"); ?>
                </a>
            </div>
        </div>

        <?php } else {
            if($cont == 1) { ?>
        <div class="w-full h-full lg:w-2/5 flex flex-col">

            <div class="group w-full relative h-full lg:h-1/2 bg-center bg-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
                <h3 class="z-[2] right-[30px] top-[30px] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180" style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
                <div class="z-[0] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
                <div class="z-[1] absolute bottom-0 p-[30px] text-white">

                    <h2 class="text-white w-2/3 font-bold mt-[10px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
                    <a class="mt-[30px] button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>"><?php _e("leggi tutto", "ambitodesign"); ?></a>
                </div>
            </div>
            </a>
            <?php } else { ?>

            <div class="group w-full relative h-full lg:h-1/2 bg-center bg-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
                <h3 class="z-[2] top-[30px] right-[30px] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180" style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
                <div class="z-[0] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
                <div class="z-[1] absolute bottom-0 p-[30px] text-white">
                    <h2 class="text-white w-2/3 font-bold mt-[10px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
                    <a class="mt-[30px] button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>"><?php _e("leggi tutto", "ambitodesign"); ?></a>
                </div>
            </div>
            </a>
        </div>
        <?php } 
    } $cont = $cont + 1; ?>

        <?php endwhile; ?>
    </div>
    <?php wp_reset_postdata(); ?>
    <?php else : ?>
    <p class="text-center text-[22px] md:text-[32px] leading-[30px] md:leading-[40px] tracking-[.68px] md:tracking-[.99px] mx-auto">
        <?php _e("Non sono presenti articoli", "ambitodesign"); ?></p>
    <?php endif; ?>
</div>
<?php endif; ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
    if($paged == 1 ) : ?>
<div class="mt-[100px] lg:hidden container responsive-container gap-y-[50px] lg:gap-[50px] grid grid-cols-1 lg:grid-cols-3 mb-[50px]">
    <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'post_status' => 'publish',
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) : ?>
    <?php while ($query->have_posts()) : $query->the_post(); ?>
    <div class="relative bg-white">
        <h3 class="left-[30px] top-[30px] z-[2] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180 " style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
        <a class="group" href="<?php echo get_permalink(); ?>">
            <div class="h-0 relative pb-[100%] lg:pb-[124%]">
                <div class="z-[1] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
                <img class="absolute w-full inset-0 h-full object-cover" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
            </div>
        </a>
        <p class="mt-[10px] text-orange text-[16px] font-medium tracking-wide uppercase"><?php echo get_the_date(); ?></p>
        <h2 class="font-bold mt-[15px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
        <a class="mt-[15px] border-orange border-solid border-2 button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>"><?php _e("leggi tutto", "ambitodesign"); ?>
        </a>
    </div> <?php
    endwhile;
    wp_reset_postdata(); 
    else : ?>
    <p class="text-center text-[22px] md:text-[32px] leading-[30px] md:leading-[40px] tracking-[.68px] md:tracking-[.99px] mx-auto">
        <?php _e("Non sono presenti articoli", "ambitodesign"); ?></p>
    <?php endif; ?>

</div>
<?php endif; ?>


<?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
     if ($paged == 1) { ?>
<div class="container responsive-container gap-y-[50px] lg:gap-[50px] grid grid-cols-1 lg:grid-cols-3">
    <?php $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'post__not_in' => wp_list_pluck($featuredQuery->posts, 'ID'),
            'paged' => $paged
        );
     } else { ?>
    <div class="mt-[100px] container responsive-container gap-y-[50px] lg:gap-[50px] grid grid-cols-1 lg:grid-cols-3">

        <?php $args = array(
            'post_type' => 'post',
            'posts_per_page' => 6,
            'post_status' => 'publish',
            'paged' => $paged
        );
     }
        $query = new WP_Query($args); ?>

        <?php while ($query->have_posts()) : $query->the_post(); ?>
        <div class="relative bg-white">
            <h3 class="left-[30px] top-[30px] z-[2] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180 " style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
            <a class="group" href="<?php echo get_permalink(); ?>">
                <div class="h-0 relative pb-[100%] lg:pb-[124%]">
                    <div class="z-[1] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
                    <img class="absolute w-full inset-0 h-full object-cover" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                </div>
            </a>
            <p class="mt-[10px] text-orange text-[16px] font-medium tracking-wide uppercase"><?php echo get_the_date(); ?></p>
            <h2 class="font-bold mt-[15px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
            <a class="mt-[15px] border-orange border-solid border-2 button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>"><?php _e("leggi tutto", "ambitodesign"); ?>
            </a>
        </div> <?php
    endwhile; ?>
        <?php wp_reset_postdata(); ?>



    </div>
    <div class="container responsive-container flex justify-center">
        <div class="pagination inline-flex items-center mt-[50px] shadow-lg">
            <?php 
        $link_prev = get_template_directory_uri().'/dist/img/pagination-arrow/pagination-left.png';
        $link_next = get_template_directory_uri().'/dist/img/pagination-arrow/pagination-right.png';
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'prev_next'    => true,
            'prev_text'    =>  __('<img class="w-[15px] h-auto" src="'.$link_prev.'" alt="">'),
            'next_text'    => __('<img class="w-[15px] h-auto" src="'.$link_next.'" alt="">')
        ) );
    ?>
        </div>
    </div>


    <?php get_footer(); ?>