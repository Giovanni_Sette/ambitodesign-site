<?php get_header(); ?>

<div class="container responsive-container pt-[200px]">
    <div class="flex flex-col justify-center items-center pt-[5rem] pb-[9rem]">
        <h1 class="text-center font-bold text-[36px] font-trajan"><?php _e("<span class='text-[70px]'>404</span><br><br>Pagina non trovata", 'ambitodesign'); ?></h1>
        <a href="<?php echo home_url(); ?>" class="mt-[50px] border-orange border-[2px] hover:bg-white hover:text-orange duration-300 transition-all text-[16px] shadow-cta_orange inline-flex py-[.9rem] px-[2.1rem] uppercase bg-orange rounded-[50px] font-medium text-white">
            Torna alla Home
        </a>
    </div>
</div>

<?php get_footer();