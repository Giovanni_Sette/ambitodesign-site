</main>
</div>

<footer id="footer" role="contentinfo">

    <div class="container responsive-container pt-[75px] pb-[25px]">

        <div class="flex md:flex-wrap lg:flex-nowrap flex-col md:flex-row justify-between w-full lg:space-x-[70px] xl:space-x-[90px] border-solid border-b border-[#707070] pb-[30px]">
            <div class="widget-1 w-full md:w-1/2 lg:w-1/4 xl:w-[25%] md:pr-[15px] lg:pr-0">
                <?php the_custom_logo();?>
                <p class="mt-[30px] text-[18px] leading-[1.4] font-medium">
                    Realizziamo il TUO sito web.
                    Crea subito la tua presenza online e inizia a monetizzare.
                </p>

            </div>

            <div class="widget-2 mt-[30px] md:mt-0 w-full md:w-1/2 lg:w-1/4 xl:w-[15%] md:pl-[40px] lg:pl-0">
                <h3 class="mb-[30px] text-[26px] leading-[1.3] font-bold">Menu</h3>
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'footer',
                        'container_class' => 'flex flex-col text-[18px] font-medium',
                        'menu_class' => '',
                        'menu_id' => '',
                        'add_li_class'  => 'mb-[10px]',
                        'add_link_class'  => 'hover:text-orange  duration-300 transition-all'
                        )); 
                    ?>
            </div>

            <div class="widget-3 mt-[30px] lg:mt-0 w-full md:w-1/2 lg:w-1/4 xl:w-[35%] md:pr-[15px] lg:pr-0">
                <h3 class="mb-[30px] text-[26px] leading-[1.3] font-bold">Ultimi articoli</h3>
                <div class="flex flex-col space-y-[10px]">
                    <?php
                        $the_query = new WP_Query(array(
                            'posts_per_page' => 3,
                            'post_type' => 'post',
                            'post_status' => 'publish',

                        ));
                    ?>
                    <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <a class="hover:text-orange text-[18px] font-medium duration-300 transition-all" href="<?php get_post_permalink(the_permalink()); ?>">
                        <?php the_title(); ?>
                    </a>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php else : ?>
                    <p><?php _e("Non sono presenti articoli", "ambitodesign"); ?></p>
                    <?php endif; ?>
                </div>
            </div>

            <div class="widget-4 mt-[30px] lg:mt-0 w-full md:w-1/2 lg:w-1/4 xl:w-[25%] md:pl-[40px] lg:pl-0">
                <h3 class="mb-[30px] text-[26px] leading-[1.3] font-bold">Contatti</h3>



                <div class="flex space-x-[12px] items-end w-full">

                    <a target="__blank" href="https://wa.me/393921163512">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/whatsapp.png" alt="Icona di Whatsapp" target="__blank">
                    </a>

                    <a href="https://www.instagram.com/ambitodesign/">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/instagram.png" alt="Icona di Instagram" target="__blank">
                    </a>

                    <a href="https://www.facebook.com/ambitodesign">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/facebook.png" alt="Icona di Facebook" target="__blank">
                    </a>

                    <a href="mailto:info@ambitodesign.it">
                        <img class="w-[28px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail" target="__blank">
                    </a>
                </div>
            </div>
        </div>
        <div class="copyright mt-[10px]">
            <div class="flex space-y-[10px] text-[15px] leading-[1.3] md:space-y-0 flex-col md:flex-row justify-between text-[#4d4d4d]">
                <p class="leading-[1.3]">Copyright 2021 - AmbitoDesign ™ | <br class="block md:hidden"> Tutti i diritti riservati</p>
                <a href="<?php echo get_permalink(3); ?>" class="text-[15px] leading-[1.3]">Privacy Policy & Cookies</a>
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>

</body>

</html>