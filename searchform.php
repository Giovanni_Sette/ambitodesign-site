<form role="search" method="get" class="search-form relative" action="<?php echo home_url( '/' ); ?>">
    <input type="search" class="search-field p-[20px] w-full focus:ring-0 focus:border-orange border-solid border-gray-400" placeholder="<?php echo esc_attr_x( 'cerca', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Cerca:', 'label' ) ?>" />

    <label>
        <i class="fas fa-search text-[20px] mt-[25px] absolute right-0 top-0 mr-[36px]"></i>
        <input type="submit" class="search-submit hidden" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
    </label>

</form>