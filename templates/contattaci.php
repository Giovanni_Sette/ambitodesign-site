<?php /* Template Name: Contattaci */ get_header(); ?>

<div class="container responsive-container pt-[100px] md:pb-[75px]">
    <div class="text-center">
        <h1 class="text-[40px] md:text-[50px] font-bold leading-[1.2]">
            Contattaci <span class="text-orange">ora!</span>
        </h1>
        <p class="mb-[50px] text-[18px] md:text-[20px] mt-[15px] font-medium leading-[1.3]">
            Parlaci del tuo progetto e dei tuoi obiettivi, <br class="hidden md:block"> saremo subito pronti ad aiutarti.
        </p>
    </div>

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-1/3 xl:w-2/5 md:pr-[25px]">
            <img class="object-cover w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/penna_tablet_pianta.jpg" alt="Illustrazione di una persona che realizza un progetto su un tablet">
        </div>
        <div class="w-full md:w-2/3 xl:w-3/5 contact-form mt-[20px] md:mt-0">
            <?php echo do_shortcode( '[contact-form-7 id="40" title="Contact Form"]' ); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>