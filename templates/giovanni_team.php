<?php /* Template Name: Giovanni */ get_header(); ?>

<div class="container responsive-container">
    <div class="xl:min-h-[720px] xl:h-[100vh] pt-[100px] pb-[50px] xl:py-[100px] xl:mx-[160px] flex flex-col lg:flex-row items-center">
        <div class="w-2/3 lg:w-2/5">
            <img class="w-full h-full object-contain" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/giovanni_sette_personal.jpg" alt="Illustrazione che mostra Giovanni Sette, componente del team ambitodesign.">
        </div>
        <div class="lg:ml-[60px] w-full lg:w-3/5">
            <h1 class="mt-[15px] lg:mt-0 mb-[30px] text-[40px] md:text-[50px] font-bold leading-[1.3]">
                Ciao, sono <br> <span class="text-orange">Giovanni Sette</span>
            </h1>

            <p class="mb-[30px] text-[18px] md:text-[20px] leading-[1.4]">
                Appassionato di design e minimalismo sin da piccolo. Ho fatto il liceo scientifico e mi piace molto
                parlare con le persone.
                Attualmente studio ITPS presso UniBa. Lavoro per sviluppare siti web efficaci che portino risultati
                concreti e che facciano felici i miei clienti.
            </p>
            <div class="flex justify-between">
                <a class="w-1/2 font-bold inline-flex items-center" href="mailto:giovsette.info@gmail.com" target="__blank">
                    <img class="w-[25px] md:w-[30px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail">
                    <p class="ml-[10px] text-[22px] ">Scrivimi</p>
                </a>
                <div class="flex space-x-[15px]">
                    <a href="https://www.linkedin.com/in/giovanni-sette/" target="__blank"><img class="w-[25px] md:w-[30px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/linkedin.png" alt="Icona di Linkedin"></a>
                    <a href="https://www.instagram.com/giovsette" target="__blank"><img class="w-[25px] md:w-[30px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/instagram.png" alt="Icona di Instagram"></a>
                </div>
            </div>

        </div>
    </div>
    <div class="mb-[60px]">
        <h2 class="mb-[30px] xl:mb-[60px] text-[40px] md:text-[50px] font-bold leading-[1.3]">
            Esperienza e Abilità
        </h2>
        <div class="flex flex-col lg:flex-row">
            <div class="mb-[30px] lg:mb-0 lg:mr-[100px] w-full lg:w-3/5 flex flex-col space-y-[30px]">
                <div class=" py-[5px] pl-[10px] border-l-[4px] border-solid border-orange">
                    <p class="text-orange text-[17px] uppercase font-bold mb-[15px]">2015 - 2020</p>
                    <h3 class="text-[24px] font-bold mb-[18px]">Liceo Scientifico </h3>
                    <div class="flex items-center">
                        <i class="text-[18px] fas fa-school"></i>
                        <p class="ml-[10px] text-[14px] md:text-[16px]">
                            Liceo Scientifico "E. Amaldi"
                        </p>
                    </div>
                </div>
                <div class=" py-[5px] pl-[10px] border-l-[4px] border-solid border-orange">
                    <p class="text-orange text-[17px] uppercase font-bold mb-[15px]">2020 - Attuale</p>
                    <h3 class="text-[24px] font-bold mb-[18px]">Informatica e Tecnologie per la Produzione del Software
                    </h3>
                    <div class="flex items-center">
                        <i class="text-[18px] fas fa-graduation-cap"></i>
                        <p class="ml-[10px] text-[14px] md:text-[16px]">
                            Università degli Studi di Bari Aldo Moro
                        </p>
                    </div>
                </div>
            </div>
            <div class="mt-[20px] w-full lg:w-2/5 flex flex-col space-y-[30px]">
                <div class="w-full bg-gray-200">
                    <div class="py-[12px] px-[20px] text-white font-bold w-[90%] bg-[#f77300d0]">
                        <p class="text-white">UI/UX Design</p>
                    </div>
                </div>
                <div class="w-full bg-gray-200">
                    <div class="py-[12px] px-[20px] text-white font-bold w-[85%] bg-[#f77300d0]">
                        <p class="text-white">Programmazione</p>
                    </div>
                </div>
                <div class="w-full bg-gray-200">
                    <div class="py-[12px] px-[20px] text-white font-bold w-[74%] bg-[#f77300d0]">
                        <p class="text-white">Sviluppo Front-End</p>
                    </div>
                </div>
                <div class="w-full bg-gray-200">
                    <div class="py-[12px] px-[20px] text-white font-bold w-[98%] bg-[#f77300d0]">
                        <p class="text-white">Parlare in pubblico</p>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php 
        $style = "background: rgb(178,63,255);background: linear-gradient(90deg, rgba(178,63,255,1) 0%, rgba(214,89,133,1) 63%, rgba(254,118,0,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div>
<?php get_footer(); ?>