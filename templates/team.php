<?php /* Template Name: Team */ get_header(); ?>

<div class="container responsive-container pt-[100px]">

    <!-- HERO -->
    <div class="mt-[25px] xl:mt-[75px] mb-[75px] xl:mb-[125px]">
        <h1 class="mb-[60px] text-[40px] md:text-[50px] text-center mx-auto w-full lg:w-3/4 xl:w-3/5 font-bold leading-[1.3]">
            Sviluppiamo siti web professionali da anni con <span class="text-orange">incredibili risultati</span>
        </h1>

        <div class="mx-auto grid grid-cols-3 grid-rows-2 md:grid-flow-col gap-[.7rem]">
            <div class="hidden md:block col-span-3 md:col-span-1 row-span-1 md:row-span-2">
                <img class="object-cover w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/persona_pc.jpg" alt="Illustrazione di una persona che lavora al pc">
            </div>
            <div class="hidden md:block col-span-3 md:col-span-1">
                <img class="object-cover w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/pc_scrivania.jpg" alt="Illustrazione di un computer portatile con mouse su una scrivania di legno">
            </div>
            <div class="col-span-3 md:col-span-1">
                <img class="object-cover w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/tablet_piantina.jpg" alt="Illustrazione con tablet e piantina">
            </div>
            <div class="col-span-3 md:col-span-1 row-span-1 md:row-span-2">
                <img class="object-cover w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/tablet_mani.jpg" alt="Illustrazione di un tablet con un progetto mockup">
            </div>
        </div>
    </div>

    <!-- LA NOSTRA STORIA -->
    <div class="flex lg:flex-row flex-col items-center">
        <div class="w-full lg:w-1/2">
            <h2 class="text-[40px] md:text-[50px] font-bold leading-[1.2]">
                La nostra <span class="text-orange">storia</span>
            </h2>

            <p class="text-[18px] md:text-[20px] mt-[30px] font-medium leading-[1.4]">
                <span class="text-orange font-bold text-[20px] md:text-[22px]">Ambitodesign</span>
                nasce nel 2019 con il preciso intento di mettersi in gioco,
                generare i primi guadagni e sviluppare competenze fondamentali per il terzo millennio.
                Muoviamo i nostri primi passi nel mondo dello sviluppo web stringendo un'importante collaborazione
                con la Web Agency <u>Worldonweb</u>, attraverso la quale applichiamo le competenze sviluppate da
                autodidatti.
                Negli anni stringiamo efficaci collaborazioni con <u>Perspective</u> ed <u>EDV Agency</u> migliorando
                notevolmente la qualità dei progetti.
            </p>
        </div>
        <div class="w-full lg:w-1/2 mt-[25px] lg:mt-0 lg:ml-[50px] xl:ml-[100px]">
            <img class="object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/chi_siamo_storia.jpg">
        </div>
    </div>


    <!-- GLI OBIETTIVI -->
    <div class="mt-[75px] lg:mt-[100px] flex lg:flex-row flex-col-reverse items-center">
        <div class="w-full lg:w-1/2">
            <img class="object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/obiettivi.jpg">
        </div>
        <div class="w-full lg:w-1/2 lg:ml-[50px] xl:ml-[100px] mt-[20px] lg:mt-0">
            <h2 class="text-[40px] md:text-[50px] font-bold">Gli <span class="text-orange">obiettivi</span></h2>
            <p class="mb-[25px] text-[18px] md:text-[20px] mt-[30px] leading-[1.4] font-medium">
                Durante la seconda metà del 2021 sentiamo la necessità di realizzare qualcosa di nostro.
                Ora vogliamo rivolgerci direttamente ai clienti, capire le loro esigenze, sviluppare soluzioni
                congeniali
                agli obiettivi e alle risorse degli stessi. Come fare a capire se saremo in grado di soddisfare le
                aspettative?
                Consulta i progetti da noi già realizzati.
            </p>
            <a href="<?php echo get_permalink(27); ?>" class="rounded-button button_slide slide_right_orange bg-orange hover:text-orange border-orange shadow_orange mb-[20px] lg:mb-0 mr-[45px]">
                Vedi Progetti
            </a>
        </div>
    </div>

    <!-- LOGHI AZIENDE COLLABORAZIONI -->
    <div class="mt-[75px] lg:mt-[100px] px-[20px] md:px-[50px] grid grid-cols-4 gap-[2rem] md:gap-[4rem]">
        <div class="col-span-2 lg:col-span-1">
            <img class="grayscale hover:grayscale-0 object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/edv.jpg"
                alt="Icona della web agency EdvWebAgency, con cui intratteniamo una collaborazione">
        </div>

        <div class="col-span-2 lg:col-span-1">
            <img class="grayscale hover:grayscale-0 object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/perspective.svg"
                alt="Icona della web agency perspective, con cui intratteniamo una collaborazione">
        </div>

        <div class="col-span-2 lg:col-span-1">
            <img class="grayscale hover:grayscale-0 object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/logo.png" alt="Logo di ambitodesign">
        </div>

        <div class="col-span-2 lg:col-span-1">
            <img class="grayscale hover:grayscale-0 object-contain w-full h-full" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/worldonweb.png"
                alt="Icona della web agency Worldonweb, con cui intratteniamo una collaborazione">
        </div>


    </div>

    <!-- IL NOSTRO TEAM -->
    <div class="mt-[75px] lg:mt-[100px]">
        <h2 class="text-center mb-[60px] text-[40px] md:text-[50px] font-bold leading-[1.4]">
            Il <span class="text-orange">nostro Team</span>
        </h2>

        <div class="team flex flex-col lg:flex-row justify-center">

            <div class="flex flex-col w-full lg:w-2/5 items-center mr-[15px]">
                <div class="w-full relative pb-[65%] h-0">
                    <img class="w-full h-full absolute inset-0 object-contain object-top" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/giovanni_sette.jpg"
                        alt="Illustrazione che mostra Giovanni Sette, componente del team ambitodesign.">
                </div>
                <div class="p-[35px]">
                    <h3 class="text-[25px] md:text-[35px] font-bold">Giovanni Sette</h3>
                    <p class="mt-[5px] text-[14px] md:text-[16px] font-medium leading-[1.4]">Web Designer & Web
                        Developer</p>
                    <div class="mt-[20px] flex space-x-[10px] items-end  text-[24px]">
                        <a href="https://www.linkedin.com/in/giovanni-sette/" target="__blank">
                            <img class="w-[25px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/linkedin.png" alt="Icona di Linkedin">
                        </a>
                        <a href="https://www.instagram.com/giovsette" target="__blank">
                            <img class="w-[25px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail">
                        </a>
                    </div>
                    <p class="text-[18px] leading-[1.4] mt-[20px]">
                        Appassionato di design e minimalismo sin da piccolo. Ho fatto il liceo scientifico e mi piace
                        molto parlare con le persone. <br>
                        Attualmente studio ITPS presso UniBa. Lavoro per sviluppare siti web efficaci che portino
                        risultati concreti e che facciano felici i miei clienti. </p>
                    <a href="<?php echo get_permalink(103); ?>" class="rounded-button button_slide slide_right_orange bg-orange hover:text-orange border-orange shadow_orange mt-[30px]">
                        Leggi di più
                    </a>

                </div>
            </div>

            <div class="mt-[50px] lg:mt-0 flex flex-col w-full lg:w-2/5 lg:ml-[15px]">
                <div class="w-full relative pb-[65%] h-0">
                    <img class="w-full h-full absolute inset-0 object-contain object-top" src="<?php echo get_template_directory_uri(); ?>/dist/img/la-nostra-storia/simone_pontrelli.jpg"
                        alt="Illustrazione che mostra Simone Pontrelli, componente del team ambitodesign.">
                </div>
                <div class="p-[35px]">
                    <h3 class="text-[25px] md:text-[35px] font-bold">Simone Pontrelli</h3>
                    <p class="mt-[5px] text-[14px] md:text-[16px] font-medium leading-[1.4]">UI Designer & Front-End
                        Developer</p>
                    <div class="mt-[20px] flex space-x-[10px] items-end  text-[24px]">
                        <a href="https://www.linkedin.com/in/simone-pontrelli-b8b64a204/" target="__blank">
                            <img class="w-[25px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/linkedin.png" alt="Icona di Linkedin">
                        </a>
                        <a href="https://www.instagram.com/simone.pontrelli" target="__blank">
                            <img class="w-[25px]" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail">
                        </a>
                    </div>
                    <p class="text-[18px] leading-[1.4] mt-[20px]">
                        Studio ITPS presso l'Università degli studi di Bari.
                        Sempre pronto a dare il meglio, vivo la mia vita una riga di codice alla volta. <br>
                        Ho sviluppato delle solide competenze nel Web Design lavorando a stretto contatto con i clienti
                        consentendomi di realizzare progetti importanti.
                    </p>
                    <a href="<?php echo get_permalink(100); ?>" class="rounded-button button_slide slide_right_orange bg-orange hover:text-orange border-orange shadow_orange mt-[30px]">
                        Leggi di più
                    </a>

                </div>

            </div>


        </div>
    </div>

    <?php 
        $style = "background: rgb(0,129,19);background: linear-gradient(103deg, rgba(0,129,19,1) 0%, rgba(20,163,80,1) 29%, rgba(93,111,254,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div>

<?php get_footer(); ?>