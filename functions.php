<?php

require_once get_template_directory() . '/vendor/setup.php';
require_once get_template_directory() . '/vendor/plugin.php';
require_once get_template_directory() . '/vendor/cpt.php';

require get_template_directory() . '/plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/Giovanni_Sette/ambitodesign-site',
	__FILE__,
	'ambitodesign-site'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('glpat-mHzDzX7LhuR8c6CfyRbo');

function theme_setup()
{

	load_theme_textdomain('theme');

	add_theme_support('custom-logo');
	add_theme_support('post-thumbnails');
	add_post_type_support('post', 'excerpt');

	register_nav_menus(array(
		'main'    => 'Main Menu',
		'footer'    => 'Footer Menu'
	));
}
add_action('after_setup_theme', 'theme_setup');

function theme_scripts()
{
	wp_enqueue_style('tailwind', get_template_directory_uri() . '/dist/css/tailwind.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('app', get_template_directory_uri() . '/dist/css/app.css', array('tailwind'), wp_get_theme()->get('Version'));

	wp_enqueue_script('script', get_template_directory_uri() . '/dist/js/script.js', array('jquery'), wp_get_theme()->get('Version'), true);
	wp_localize_script('script', 'ajaxurl', array(admin_url('admin-ajax.php')));
	wp_localize_script('script', 'directory', array(get_template_directory_uri()));
}
add_action('wp_enqueue_scripts', 'theme_scripts');

if ( ! function_exists( 'number_pagination' ) ) :
    function number_pagination() {
        global $wp_query;

        $big = 999999999; 

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages
        ) );
    }
endif;