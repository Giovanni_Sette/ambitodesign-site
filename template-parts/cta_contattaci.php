<div class="mt-[75px] lg:my-[100px] overflow-hidden relative text-white" style="<?php echo $style; ?>">
    <div class="z-[0] absolute w-full h-full bg-gray-700 bg-opacity-20"></div>
    <div class="z-[1] relative cta_contattaci ">
        <h2 class="text-white font-bold mb-[25px] text-[25px] md:text-[35px] leading-[1.2]">Hai idea di quanto un sito web <br>possa incrementare le tue entrate?</h2>
        <p class="text-white font-medium text-[18px] md:text-[20px] leading-[1.4] mb-[30px]">Contattaci e scopri tutti i motivi per realizzare il tuo sito web ora.</p>
        <a href="<?php echo get_permalink(13); ?>" class="button_slide slide_right_orange rectangle-button">
            <?php _e("Contattaci ora", "ambitodesign"); ?>
        </a>
    </div>
</div>