<?php get_header(); ?>
<div class="container responsive-container pt-[100px]">


    <h1 class="text-[40px] md:text-[50px] font-bold leading-[1.3] mb-[50px]">
        <?php the_title(); ?>
    </h1>
    <div class="prose">
        <?php the_content();?>
    </div>
</div>

<?php get_footer();