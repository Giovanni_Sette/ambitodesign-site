<?php get_header(); ?>

<div class="container responsive-container">

    <div class="pt-[100px] flex flex-col xl:flex-row xl:gap-x-[50px]">

        <div class="w-full xl:w-3/4">
            <?php if (is_author()) : ?>
            <h1 class="text-[25px] md:text-[35px] font-bold mb-[50px]"><?php _e("Risultati per autore: ", "ambitodesign"); echo get_the_author_meta( 'nickname' ); ?> </h1>
            <?php endif; ?>
            <?php if (is_category()) : ?>
            <h1 class="text-[25px] md:text-[35px] font-bold mb-[50px]"><?php _e("Risultati per categoria: ", "ambitodesign"); echo single_cat_title(); ?> </h1>
            <?php endif; ?>
            <?php if (is_date()) : ?>
            <h1 class="text-[25px] md:text-[35px] font-bold mb-[50px]"><?php _e("Risultati per data: ", "ambitodesign"); echo get_the_date('F Y');?> </h1>
            <?php endif; ?>
            <?php if (is_tag()) : ?>
            <h1 class="text-[25px] md:text-[35px] font-bold mb-[50px]"><?php _e("Risultati per tag: ", "ambitodesign"); echo single_tag_title(); ?> </h1>
            <?php endif; ?>

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-y-[50px] lg:gap-x-[50px]">
                <?php if (have_posts()) :
                while (have_posts()) : the_post(); ?>

                <div class="relative bg-white">
                    <h3 class="left-[30px] top-[30px] z-[2] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180 " style="writing-mode: vertical-lr"><?php the_category() ?></h3>
                    <a class="group" href="<?php echo get_permalink(); ?>">
                        <div class="h-0 relative pb-[100%] lg:pb-[124%]">
                            <div class="z-[1] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700"></div>
                            <img class="absolute w-full inset-0 h-full object-cover" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                        </div>
                    </a>
                    <p class="mt-[10px] text-orange text-[16px] font-medium tracking-wide uppercase"><?php echo get_the_date(); ?></p>
                    <h2 class="font-bold mt-[15px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
                    <a class="mt-[15px] border-orange border-solid border-2 button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>"><?php _e("leggi tutto", "ambitodesign"); ?>
                    </a>
                </div>

                <?php endwhile; 
                wp_reset_postdata(); 
                else : ?>

                <p class="text-center text-[22px] md:text-[32px] leading-[30px] md:leading-[40px] tracking-[.68px] md:tracking-[.99px] mx-auto">
                    <?php _e("Non sono presenti articoli", "ambitodesign"); ?></p>

                <?php endif; ?>
            </div>

        </div>

        <div class="w-full xl:w-1/4 mt-[50px] xl:mt-0">
            <?php include(locate_template('sidebar.php')); ?>
        </div>

    </div>

    <?php 
        $style = "background: rgb(178,63,255);background: linear-gradient(90deg, rgba(178,63,255,1) 0%, rgba(214,89,133,1) 63%, rgba(254,118,0,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div>

<?php get_footer(); ?>