<?php get_header(); ?>
<div class="container flex responsive-container xl:h-[100vh] xl:min-h-[720px] pt-[100px] xl:py-[100px]">

    <div class="flex flex-col-reverse xl:flex-row md:items-center w-full">

        <div class="animation-button w-full xl:w-[56%]">
            <h2 class="hidden xl:block uppercase text-orange text-[19px] font-bold tracking-wide mb-[30px]">Diamo forma
                alla tua idea</h2>
            <h1 class="mt-[15px] xl:mt-0 font-bold text-[40px] md:text-[55px] lg:text-[65px] mb-[30px] leading-[1.15]">
                Creiamo il sito web adatto alle tue esigenze</h1>
            <p class="xl:w-3/4 font-medium text-[18px] md:text-[20px] xl:text-[22px] leading-[1.4] mb-[30px]">
                Aiutiamo liberi professionisti ed aziende a far crescere il loro business.
                Lavoriamo per espandere la tua attività un pixel alla volta.
            </p>
            <a href="<?php echo get_permalink(9); ?>" class="rounded-button button_slide slide_right_orange bg-orange hover:text-orange border-orange shadow_orange mb-[20px] md:mb-0 mr-[45px]">
                Perchè scegliere noi?
            </a>
            <a href="<?php echo get_permalink(13); ?>" class="rounded-button button_slide slide_right_green bg-green hover:text-green border-green shadow_green mb-[20px] md:mb-0 mr-[45px]">
                Contattaci Ora
            </a>
        </div>

        <div class="w-full mx-auto xl:w-[34%]">

            <h2 class="xl:hidden hidden md:block uppercase text-orange text-[19px] font-bold tracking-wide mb-[30px]">
                Diamo forma
                alla tua idea</h2>
            <div class="hidden md:block md:w-[50%] lg:w-[63%] xl:w-full mx-auto">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/hero1.jpg" class="w-full" alt="Questa illustrazione mostra due smartphone il cui sfondo è il logo di ambitodesign">
            </div>
            <div class="md:hidden md:w-[65%] xl:w-full mx-auto">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/hero.jpg" class="w-full" alt="Questa illustrazione mostra tre smartphone il cui sfondo è il logo di ambitodesign">
            </div>

        </div>

    </div>

</div>


<div class="container responsive-container ">

    <!-- PERCHE NOI -->
    <h2 class="mt-[75px] lg:mt-[100px] xl:mt-0 font-bold text-[35px] text-center md:text-[45px] xl:text-[50px] leading-[1.2] mb-[30px] md:mb-[75px]">
        Perchè proprio noi?</h2>

    <div class="xl:mb-[200px] text-center flex flex-col lg:flex-row space-y-[40px] lg:space-y-0 lg:space-x-[50px]">

        <div class="w-full mx-auto md:w-3/5 lg:w-1/3">

            <span class="mx-auto relative flex items-center justify-center rounded-full w-[125px] h-[125px]">
                <svg class="absolute z-[-1]" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#FE7600"
                        d="M45.4,-73.5C57.7,-62.7,65.8,-48.2,73.2,-33.1C80.6,-18,87.3,-2.3,87.1,13.9C87,30.1,80,46.6,67.8,56.4C55.7,66.2,38.5,69.3,21.8,74C5.2,78.8,-10.9,85.2,-25,82.3C-39.1,79.4,-51.3,67.2,-63,54.3C-74.6,41.3,-85.8,27.6,-87.4,12.9C-89.1,-1.9,-81.3,-17.7,-72.7,-31.9C-64.2,-46,-54.8,-58.4,-42.6,-69.2C-30.3,-80,-15.2,-89.1,0.7,-90.2C16.5,-91.3,33.1,-84.2,45.4,-73.5Z"
                        transform="translate(100 100)" />
                </svg>
                <img class="w-[50px] invert" src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/produttivita.png" alt="Icona che rappresenta la produttività">
            </span>

            <h3 class="mt-[10px] font-bold text-[28px]">Produttività</h3>
            <p class="text-[18px] font-medium leading-[1.4] mt-[30px]">Siamo dei grandi stacanovisti, lavoriamo
                assiduamente per soddisfare le aspettative dei nostri clienti.</p>

        </div>

        <div class="w-full mx-auto md:w-3/5 lg:w-1/3">

            <span class="mx-auto relative flex items-center justify-center rounded-full w-[125px] h-[125px]">
                <svg class="absolute z-[-1]" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#FE7600"
                        d="M49,-72.7C63.5,-67,75.1,-53.3,78.5,-38.1C81.8,-23,76.9,-6.3,72.2,8.8C67.6,23.9,63.2,37.4,55.9,51.6C48.6,65.8,38.3,80.5,24.8,84.4C11.4,88.4,-5.3,81.4,-22.5,76.4C-39.7,71.5,-57.6,68.4,-69.1,58.2C-80.6,48,-85.8,30.5,-85.2,14.2C-84.5,-2.2,-78,-17.4,-71,-32.3C-64,-47.1,-56.4,-61.6,-44.5,-68.4C-32.6,-75.3,-16.3,-74.6,0.5,-75.4C17.3,-76.2,34.6,-78.5,49,-72.7Z"
                        transform="translate(100 100)" />
                </svg>
                <img class="w-[50px] invert" src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/flessibilita.png" alt="Icona che rappresenta la flessibilità">
            </span>


            <h3 class="mt-[10px] font-bold text-[28px]">Flessibilità</h3>
            <p class="text-[18px] font-medium leading-[1.4] mt-[30px]">Comunichiamo in maniera chiara con i nostri
                clienti, riuscendo ad essere flessibili dall'idea al progetto finale.</p>

        </div>

        <div class="w-full mx-auto md:w-3/5 lg:w-1/3">

            <span class="mx-auto relative flex items-center justify-center rounded-full w-[135px] h-[135px]">
                <svg class="absolute z-[-1]" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#FE7600"
                        d="M42.5,-60.2C56.7,-48.2,70.8,-37.9,79.5,-23.1C88.1,-8.2,91.2,11.2,83.8,25.1C76.4,39,58.5,47.4,42.7,54.7C27,62,13.5,68.2,-2.4,71.4C-18.2,74.7,-36.4,74.9,-47.1,66C-57.7,57.1,-60.8,38.9,-66,21.8C-71.1,4.6,-78.3,-11.6,-74.9,-25.1C-71.5,-38.6,-57.6,-49.4,-43.4,-61.3C-29.2,-73.2,-14.6,-86.3,-0.2,-86C14.2,-85.7,28.4,-72.1,42.5,-60.2Z"
                        transform="translate(100 100)" />
                </svg>

                <img class="w-[50px] invert" src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/professionalita.png" alt="Icona che rappresenta la professionalità">
            </span>

            <h3 class="mt-[10px] font-bold text-[28px]">Professionalità</h3>
            <p class="text-[18px] font-medium leading-[1.4] mt-[30px]">Realizziamo siti web professionali e veloci.
                Siamo molto scrupolosi riguardo il design, l'esperienza utente e la progettazione.</p>

        </div>
    </div>

    <!-- TEAM -->
    <div class="mt-[75px] lg:mt-[100px] flex flex-col-reverse lg:flex-row items-center w-full justify-center">
        <div class="w-full lg:w-[50%] flex lg:mr-[60px] mt-[30px] lg:mt-0">

            <div>
                <h3 class="self-start uppercase text-orange text-[18px] font-bold tracking-wide mb-[5px]">Team</h3>
                <h2 class="font-bold text-[40px] md:text-[50px] leading-[1.2] mb-[30px] xl:mb-[50px]">Chi Siamo</h2>

                <p class="font-medium text-[18px] md:text-[20px] leading-[1.4]">
                    Siamo un gruppo di web designer molto giovani ed intraprendenti. Sempre pronti a scoprire nuove
                    tecnologie, migliorandoci e mettendoci in gioco, collaboriamo con numerose agenzie web da anni. <br>
                </p>
                <p class="text-black text-[18px] md:text-[20px] leading-[1.4] mt-[30px] mb-[20px] font-medium">
                    Clicca qui per scoprire di più sulla nostra storia.
                </p>
                <a href="<?php echo get_permalink(9); ?>" class="rounded-button button_slide slide_right_green bg-green hover:text-green border-green shadow_green mb-[20px] md:mb-0">
                    la nostra storia
                </a>
            </div>
        </div>

        <div class="w-full lg:w-[50%] z-[-1] relative">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/chi_siamo.jpg" class="w-full"
                alt="Questa illustrazione mostra i due componenti del team di ambitodesign: Giovanni Sette (a sinistra) e Simone Pontrelli (a destra)">
        </div>
    </div>

    <!-- PORTFOLIO -->
    <div class="mt-[75px] lg:mt-[100px] flex flex-col lg:flex-row items-center w-full justify-center">
        <div class="w-full lg:w-[50%] z-[-1] relative">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/tablet.jpg" class="w-full" alt="Questa illustrazione mostra due tablet che stanno navigando su un sito web realizzato da ambitodesign">
        </div>

        <div class="w-full lg:w-[50%] lg:ml-[60px] flex mt-[30px] lg:mt-0">
            <div>
                <h3 class="self-start uppercase text-orange text-[18px] font-bold tracking-wide mb-[5px]">Portfolio</h3>
                <h2 class="font-bold text-[40px] md:text-[50px] mb-[30px] xl:mb-[50px] leading-[1.2]">I nostri progetti
                </h2>
                <p class="font-medium text-[18px] md:text-[20px] leading-[1.4]">
                    Durante il nostro percorso lavorativo abbiamo avuto la possibilità di realizzare numerosi progetti,
                    permettendoci di acquisire competenze nuove e migliorare
                    la qualità della progettazione e comunicazione del prodotto finale.
                </p>
                <p class="text-black text-[18px] md:text-[20px] leading-[1.4] mt-[30px] mb-[20px] font-medium">
                    Clicca qui per visionare il nostro portfolio.
                </p>
                <a href="<?php echo get_permalink(27); ?>" class="rounded-button button_slide slide_right_green bg-green hover:text-green border-green shadow_green mb-[20px] md:mb-0">
                    Tutti i Progetti
                </a>
            </div>
        </div>
    </div>

    <!-- BLOG -->
    <div class="mt-[75px] lg:mt-[100px] w-full">
        <h2 class="font-bold text-[40px] md:text-[50px] mb-[50px]">Blog</h2>
        <div class="gap-y-[50px] lg:gap-[50px] grid grid-cols-1 lg:grid-cols-3">
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'post_status' => 'publish',
            );
            $query = new WP_Query($args);
            if ($query->have_posts()) : ?>
            <?php while ($query->have_posts()) : $query->the_post(); ?>

            <div class="relative bg-white">
                <h3 class="left-[30px] top-[30px] z-[2] uppercase text-orange text-[19px] font-bold tracking-wide absolute rotate-180 " style="writing-mode: vertical-lr"><?php $category = get_the_category(); 
                echo $category[0]->cat_name; ?> </h3>
                <a class="group" href="<?php echo get_permalink(); ?>">
                    <div class="h-0 relative pb-[100%] lg:pb-[124%]">
                        <div class="z-[1] group-hover:opacity-60 absolute w-full h-full bg-black opacity-50 transition-all duration-700">
                        </div>
                        <img class="absolute w-full inset-0 h-full object-cover" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                    </div>
                </a>
                <p class="mt-[10px] text-orange text-[16px] font-medium tracking-wide uppercase">
                    <?php echo get_the_date(); ?></p>
                <h2 class="font-bold mt-[15px] text-[26px] leading-[1.3]"><?php the_title(); ?></h2>
                <a class="mt-[15px] border-orange border-solid border-2 button_slide slide_right_orange rectangle-button" href="<?php echo get_permalink(); ?>">
                    <?php _e("leggi tutto", "ambitodesign"); ?>
                </a>
            </div>

            <?php endwhile;
            wp_reset_postdata(); 
            else : ?>
            <p class="text-center text-[22px] md:text-[32px] leading-[30px] md:leading-[40px] tracking-[.68px] md:tracking-[.99px] mx-auto">
                <?php _e("Non sono presenti articoli", "ambitodesign"); ?>
            </p>
            <?php endif; ?>
        </div>
    </div>

    <!-- CTA CONTATTACI -->
    <?php 
        $style = "background: rgb(80,23,159);background: linear-gradient(77deg, rgba(80,23,159,1) 0%, rgba(182,40,168,1) 53%, rgba(232,41,154,1) 100%);";
        include(locate_template('template-parts/cta_contattaci.php')); 
    ?>

</div> <!-- fine CONTAINER -->

<?php get_footer(); ?>