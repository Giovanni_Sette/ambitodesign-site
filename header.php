<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#fe7600" />

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title><?php echo wp_get_document_title(); ?></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VNTL4DK281"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VNTL4DK281');
    </script>
    <?php wp_head(); ?>

</head>

<body <?php body_class("flex flex-col min-h-screen"); ?>>

    <?php
	wp_body_open();
	?>
    <div id="preloader" class="w-full h-screen min-h-[300px] fixed inset-0 z-[998] bg-white border-solid border-orange border-[5px]">

        <div class="flex w-full h-full flex-col space-y-[15px] justify-center items-center">
            <?php the_custom_logo();?>

            <div class="dot-falling"></div>
        </div>



    </div>

    <header id="header" class="bg-white fixed w-full z-[100]" role="banner">

        <div class="container responsive-container py-[1.5rem] flex items-center justify-between">

            <div class="logo z-30">
                <?php the_custom_logo();?>
            </div>

            <div class="z-30 block">
                <button class="hamburger" type="button">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>

            <div class="bg-white fixed fixed-mobile inset-0 transition-all duration-500 opacity-0 invisible z-[20] overflow-hidden js-menu-wrapper flex">
                <div class="container justify-between flex flex-col lg:flex-row items-center px-0 lg:px-[2rem] xl:px-0">
                    <div class="hidden lg:block w-2/5 2xl:w-[45%] z-[-1] relative mr-[50px]">
                        <div class="relative w-full top-0">
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/homepage/hero1.jpg" class="w-full img-header" alt="Questa illustrazione mostra due smartphone il cui sfondo è il logo di ambitodesign">
                            </div>
                        </div>
                    </div>
                    <div class="w-full lg:w-[45%] mt-[150px] lg:mt-0">
                        <?php wp_nav_menu(
                    array(
                        'theme_location' => 'main',
                        'container_class' => 'm-auto',
                        'menu_class' => 'main-menu px-[1rem] md:px-[2rem] lg:px-0 space-y-[30px] md:space-y-[50px]',
                        'menu_id' => 'main-menu',
                        'add_li_class'  => '',
                        'add_link_class'  => 'leading-none hover:text-orange duration-300 transition-all ease-in text-[40px] md:text-[50px] font-bold relative pb-[3px]'
                        )); 
                    ?>

                        <div class="z-[1] hidden lg:flex social gap-x-[14px] mt-[50px]">
                            <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://www.instagram.com/ambitodesign/" target="__blank">
                                <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/instagram.png" alt="Icona di Instagram">
                            </a>
                            <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://www.facebook.com/ambitodesign" target="__blank">
                                <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/facebook.png" alt="Icona di Facebook">
                            </a>
                            <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://wa.me/393921163512" target="__blank">
                                <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/whatsapp.png" alt="Icona di Whatsapp">
                            </a>
                            <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="mailto:info@ambitodesign.it" target="__blank">
                                <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail">
                            </a>
                        </div>

                    </div>
                    <div class="z-[1] flex lg:hidden social justify-center gap-x-[14px] mt-auto mb-[50px]">
                        <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://www.instagram.com/ambitodesign/" target="__blank">
                            <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/instagram.png" alt="Icona di Instagram">
                        </a>
                        <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://www.facebook.com/ambitodesign" target="__blank">
                            <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/facebook.png" alt="Icona di Facebook">
                        </a>
                        <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="https://wa.me/393921163512" target="__blank">
                            <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/whatsapp.png" alt="Icona di Whatsapp">
                        </a>
                        <a class="shadow-lg transition-all duration-300 rounded-full group bg-white hover:bg-orange flex justify-center items-center w-[55px] h-[55px]" href="mailto:info@ambitodesign.it" target=" __blank">
                            <img class="w-[25px] group-hover:invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/gmail.png" alt="Icona di Gmail">
                        </a>
                    </div>
                </div>
            </div>



        </div>

    </header>

    <div id="primary">
        <main id="main" role="main">
            <div class="z-[999] cursor-dot-outline"></div>
            <div class="z-[999] cursor-dot"></div>

            <a class="xl:hidden fixed right-[20px] bottom-[20px] z-[50] shadow-lg transition-all duration-300 rounded-full group bg-green flex justify-center items-center w-[55px] h-[55px]" href="https://wa.me/393921163512"
                target="__blank">
                <img class="w-[25px] invert transition-all duration-300" src="<?php echo get_template_directory_uri(); ?>/dist/img/social-icon/whatsapp.png" alt="Icona di Whatsapp">
            </a>